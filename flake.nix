{
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (
        system:
          let
            pkgs = nixpkgs.legacyPackages.${system};
          in
            {
              devShell = with pkgs; mkShell {
                buildInputs = [
                  dotnet-sdk_6
                  zip
                  gettext
                ];
              };
            }
      );
}
